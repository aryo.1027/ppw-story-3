from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('edu/', views.edu, name='edu'),
]