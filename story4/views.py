from django.shortcuts import render

# Create your views here.

def index(request):
    return render (request,'story4/index.html')

def about(request):
    return render (request,'story4/about.html')

def edu(request):
    return render (request,'story4/edu.html')