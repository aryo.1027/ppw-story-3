from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect,  get_object_or_404
from django.http import HttpResponseRedirect
from .models import Subject
from .forms import SubjectForm
import datetime

# Create your views here.


def subject(request):
    if request.method == "POST":
        Subject.objects.get(id=request.POST['id']).delete()
        return redirect('/subject/')
    subject_list = Subject.objects.all()
    return render(request, 'subject_list.html', {'subject_list': subject_list})


def subject_add(request):
    submit = False
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            # return render(request, 'subject_add.html')
            return HttpResponseRedirect('/subject/add/?submit=True')
    return render(request, 'subject_add.html', {'form': form, 'submit': submit})

def subject_detail(request, pk):
    matkul = get_object_or_404(Subject, id=pk)
    return render(request, 'story5:subject_detail', {'post': matkul})
